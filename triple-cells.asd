(asdf:defsystem #:triple-cells
  :name "triple-cells"
  :author "Kenny Tilton"
  :version "1.0"
  :licence ""
  :depends-on (#:cells)
  :serial t
  :components
  ((:file "defpackage")
   (:file "ag-utilities")
   (:file "3c-integrity")
   (:file "core")
   (:file "api")
   (:file "dataflow")
   (:file "observer")
   (:file "hello-world")
   (:file "read-me")))
