;; -*- mode: Lisp; Syntax: Common-Lisp; Package: triple-cells; -*-
;;;
;;;
;;; Copyright (c) 2008 by Kenneth William Tilton.
;;;


(in-package :3c)

;; --- ag utils -----------------------

#+test
(progn
  (make-tutorial-store)
  (let ((s (mk-upi "a"))
        (p (new-blank-node)))
    (loop repeat 10
          do (add-triple s (mk-upi (random 10)) p))
    (index-new-triples)
    (loop for tr in (get-triples-list :s s)
        do (print (upi->value (predicate tr))))))

(defun triple-value (tr)
  (when tr
    (upi->value (object tr))))

(defun get-sp (s p)
  #+allegrocl (get-triple :s s :p p)
  #-allegrocl (car (get-triples-list :s s :p p)))

(defun get-spo (s p o)
  #+allegrocl (get-triple :s s :p p :o o)
  #-allegrocl (car (get-triples-list :s s :p p :o o)))

(defun get-sp-value (s p)
  (triple-value (get-sp s p)))

(defun mk-upi (v)
  (typecase v
    (string (literal v))
    (symbol (mk-upi (symbol-name v)))
    (integer (value->upi v :long))
    (future-part v)
    (otherwise (if (upip v) v
                 (break "not upi-able ~a ~a" (type-of v) v)))))


(defun ensure-triple (s p o)
  (unless (get-spo s p o)
    (add-triple s p o)))

